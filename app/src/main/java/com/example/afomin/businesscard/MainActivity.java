package com.example.afomin.businesscard;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.buttonSendMessage).setOnClickListener(this);
        findViewById(R.id.imageButtonShareHabr).setOnClickListener(this);
        findViewById(R.id.imageButtonShareTelegram).setOnClickListener(this);
        findViewById(R.id.imageButtonShareTwitter).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case (R.id.buttonSendMessage): {
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:"));
                intent.putExtra(Intent.EXTRA_EMAIL, new String[] {getResources().getString(R.string.my_email)});
                intent.putExtra(Intent.EXTRA_SUBJECT, "Feedback from business card");
                intent.putExtra(Intent.EXTRA_TEXT, ((EditText)findViewById(R.id.editTextForMessage)).getText());
                if (intent.resolveActivity(getPackageManager())== null) {
                    Toast.makeText(this, getResources().getString(R.string.no_email_app_installed), Toast.LENGTH_SHORT).show();
                    return;
                }
                startActivity(intent);
                break;
            } case (R.id.imageButtonShareHabr): {
                callBrowser("https://habr.com");
                break;
            } case (R.id.imageButtonShareTelegram): {
                callBrowser("https://telegram.org/");
                break;
            } case (R.id.imageButtonShareTwitter): {
                callBrowser("https://twitter.com");
                break;
            } default: {
                // do nothing
                break;
            }
        }
    }
    private void callBrowser(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        if (intent.resolveActivity(getPackageManager()) == null) {
            Toast.makeText(this, getResources().getString(R.string.no_browser_app_installed), Toast.LENGTH_SHORT).show();
            return;
        }
        startActivity(intent);
    }
}
